import config from 'config';
import { connect, IClientOptions } from 'mqtt';
import logger from '../logger';
import getOnMQTTMessage from './communications/getOnMQTTMessage';

const options: IClientOptions = {
    // Clean session
    clean: true,
    connectTimeout: 4000,
    // Auth
    clientId: 'emqx_test',
    username: 'emqx_test',
    password: 'emqx_test',
};

const getMqtt = () => {
    const client = connect(config.get('mqtt.server'), options);

    // eslint-disable-next-line no-unused-vars
    client.on('connect', (_CONNAKG) => {
        logger.info(`MQTT: Connection established`);
        // logger.info(`CONNAKG${JSON.stringify(CONNAKG)}`);
        client.subscribe(
            config.get('mqtt.subscribe'),
            { qos: config.get('mqtt.qos') },
            (err) => {
                if (err) {
                    logger.error(
                        'MQTT: Failed to subscribe to topic.',
                    );
                } else {
                    logger.info(
                        `MQTT: Subscribed to: ${config.get(
                            'mqtt.subscribe',
                        )}`,
                    );
                }
            },
        );
    });

    const onMQTTMessage = getOnMQTTMessage();

    client.on('message', (topic, message) => {
        logger.info(`MQTT message: ${message.toString()}`);
        // onMQTTMessage(message.toString());
        onMQTTMessage(topic, message);
    });

    client.on('close', () => {
        logger.info('MQTT: Connection closed by client');
    });

    client.on('reconnect', () => {
        logger.info('MQTT: Client trying a reconnection');
    });

    client.on('offline', () => {
        logger.info('MQTT: Client is currently offline');
    });

    client.on('error', (error) => {
        logger.error(error);
    });

    return client;
};

export default getMqtt;
