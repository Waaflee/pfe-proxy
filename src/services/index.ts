import { MqttClient } from 'mqtt';

export interface Connection {
    // eslint-disable-next-line no-unused-vars
    write(message: string): void;
}

type MQTTClient = Pick<MqttClient, 'publish'>;

export interface Services {
    mqtt?: MQTTClient;
    ws?: Connection;
}

const services: Services = {};
export type ServiceFactory = () => Services;

export const getServices: ServiceFactory = () => services;
export const setWs = (_ws?: Connection) => {
    services.ws = _ws;
};

export default services;
