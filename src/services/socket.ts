import ws from 'sockjs';
import config from 'config';
import logger from '../logger';
import getOnSocketMessage from './communications/getOnSocketMessage';
import { setWs } from '.';

const options: ws.ServerOptions = {
    prefix: config.get('server.prefix'),
};

const onSocketMessage = getOnSocketMessage();

const getSocket = (registerConnection = setWs) => {
    const socket = ws.createServer(options);

    socket.on('connection', (connection) => {
        logger.info('WS: Connection established');
        registerConnection(connection);

        connection.on('data', (message) => {
            logger.info(`WS: Message received: ${message}`);
            onSocketMessage(message);
        });
        connection.on('close', () => {
            logger.info('WS: Connection closed');
            registerConnection();
        });
    });
    return socket;
};

export default getSocket;
