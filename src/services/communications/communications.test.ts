import config from 'config';
import logger from '../../logger';
import getOnMQTTMessage from './getOnMQTTMessage';
import getOnSocketMessage from './getOnSocketMessage';

logger.level = 'error';

const mockSocketWrite = jest.fn((message: string) => message);
const fakeSocketServices = () => ({
    ws: { write: mockSocketWrite },
});

const onMQTTMessage = getOnMQTTMessage(fakeSocketServices);

describe('Communications/MQTT', () => {
    it('Should Forward MQTT messages to Websocket Service', () => {
        const message = JSON.stringify({ mqtt: 'message' });

        onMQTTMessage('test/incomingMessage', Buffer.from(message));

        expect(mockSocketWrite.mock.calls.length).toBe(1);
        expect(mockSocketWrite.mock.calls[0][0]).toStrictEqual(
            message,
        );
    });
});

const mockMqttPublish = jest.fn(
    (_topic: string, message: string) => message as any,
);
const fakeMqttServices = () => ({
    mqtt: { publish: mockMqttPublish },
});

const onSocketMessage = getOnSocketMessage(fakeMqttServices);

describe('Communications/WebSocketSocket', () => {
    it('Should Forward WebSocket messages to MQTT Service', () => {
        const message = JSON.stringify({ webSocket: 'message' });

        onSocketMessage(message);

        expect(mockMqttPublish.mock.calls.length).toBe(1);
        expect(mockMqttPublish.mock.calls[0][0]).toStrictEqual(
            config.get('mqtt.publish'),
        );
        expect(mockMqttPublish.mock.calls[0][1]).toStrictEqual(
            message,
        );
    });
});
