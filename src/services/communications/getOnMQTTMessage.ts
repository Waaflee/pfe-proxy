import { ServiceFactory, getServices } from '..';
import logger from '../../logger';

export interface MqttMessage {
    robotID?: number;
    translationDelta: number;
    rotationDelta: number;
}

const repackMessage = (message: Buffer): string => {
    const data: MqttMessage = JSON.parse(message.toString());
    return JSON.stringify(data);
};

const getOnMQTTMessage =
    (serviceFactory: ServiceFactory = getServices) =>
    (_topic: string, message: Buffer) => {
        const { ws } = serviceFactory();

        if (ws) {
            const payload = repackMessage(message);
            logger.info('MQTT: Forwarding to WS');
            ws.write(payload);
        } else {
            logger.error(
                'MQTT: message received but no socket connection is available',
            );
        }
    };

export default getOnMQTTMessage;
