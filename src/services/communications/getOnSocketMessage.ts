import config from 'config';
import { getServices, ServiceFactory } from '..';
import logger from '../../logger';

export interface SocketMessage {
    robotID?: number;
    translationTarget: number;
    rotationTarget: number;
}

const getOnSocketMessage =
    (serviceFactory: ServiceFactory = getServices) =>
    (message: string) => {
        const { mqtt } = serviceFactory();

        if (mqtt) {
            mqtt.publish(config.get('mqtt.publish'), message, {
                qos: config.get('mqtt.qos'),
            });
        } else {
            logger.error(
                'WebSocket: message received but no mqtt connection is available',
            );
        }
    };

export default getOnSocketMessage;
