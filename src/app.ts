import http from 'http';
import services from './services';
import getMqtt from './services/mqtt';
import getSocket from './services/socket';

const mqtt = getMqtt();
const socket = getSocket();

services.mqtt = mqtt;

const app = http.createServer();
socket.installHandlers(app);

export default app;
