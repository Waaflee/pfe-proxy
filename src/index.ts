import config from 'config';
import app from './app';
import logger from './logger';

app.listen(config.get('server.port'), config.get('server.host'));

process.on('unhandledRejection', (reason, p) =>
    logger.error('Unhandled Rejection at: Promise ', p, reason),
);

// process.on('SIGTERM', () => {
//     logger.info('SIGTERM received');
//     app.removeAllListeners();
//     app.close(() => {
//         process.exit(0);
//     });
// });

// process.on('SIGKILL', () => {
//     logger.warn('SIGKILL received');
//     process.exit(1);
// });
