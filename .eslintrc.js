module.exports = {
    env: {
        es2021: true,
        node: true,
        jest: true,
    },
    extends: ['airbnb-base', 'eslint:recommended', 'prettier'],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
    settings: {
        'import/resolver': {
            node: {
                paths: ['src'],
                extensions: ['.js', '.ts'],
                moduleDirectory: ['node_modules/', 'src/'],
            },
        },
    },
    plugins: ['@typescript-eslint', 'prettier'],
    rules: {
        'prettier/prettier': ['error'],
        'import/extensions': [
            'warn',
            'never',
            {
                json: 'always',
            },
        ],
    },
};
