module.exports = {
    server: {
        port: 8080,
        host: '0.0.0.0',
        prefix: '/proxy',
    },
    mqtt: {
        server: 'mqtt://broker.emqx.io:1883',
        port: 1883,
        qos: 2,
        publish: 'pfe/map2',
        subscribe: 'pfe/robot2',
    },
};
